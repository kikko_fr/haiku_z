class GeomUtils

  @getSubdivPts = (inpts, steps=10) ->
    out = []
    for i in [1...inpts.length]
      prev = inpts[i-1]
      curr = inpts[i]
      for j in [0...steps]
        x = (curr.x*j + prev.x*(steps-j)) / steps
        y = (curr.y*j + prev.y*(steps-j)) / steps
        pt = new paper.Point x,y
        out.push pt
    out.push out[0].clone()
    return out

  @getPolyPts = (x,y,radius=300,steps=6,thetaOffet=-Math.PI*0.5) ->
    radius *= 0.6 if $(window).width() < 640
    arr = [0...steps]
    arr.push 0
    poly = []
    for i in arr
      theta = i / steps * Math.PI * 2 + thetaOffet
      px = Math.cos(theta) * radius
      py = Math.sin(theta) * radius
      poly.push new paper.Point(x+px, y+py)
    # if steps != 60
    #   poly = GeomUtils.getSubdivPts poly, 60 / steps
    return poly

  @getCirclePts = (x,y,radius=300,steps=60,thetaOffet=-Math.PI*0.5) ->
    radius *= 0.6 if $(window).width() < 640
    arr = [0...steps]
    arr.push 0
    circle = []
    for i in arr
      theta = i / steps * Math.PI * 2 + thetaOffet
      px = Math.cos(theta) * radius
      py = Math.sin(theta) * radius
      circle.push new paper.Point(x+px, y+py)
    # if steps != 60
    #   circle = GeomUtils.getSubdivPts circle, 60 / steps
    return circle

  @getStarPts = (x,y,innerRadius=200,outerRadius=300, steps=10,thetaOffet=-Math.PI*0.5) ->
    if $(window).width() < 640
      innerRadius *= 0.6
      outerRadius *= 0.6
    steps -= 1 if steps%2
    arr = [0...steps]
    arr.push 0
    star = []
    for i in arr
      theta = i / steps * Math.PI * 2 + thetaOffet
      radius = if i%2 then innerRadius else outerRadius
      px = Math.cos(theta) * radius
      py = Math.sin(theta) * radius
      star.push new paper.Point(x+px, y+py)
    # pts = GeomUtils.getSubdivPts star, 10
    return star

  @getRectPts = (x,y,w=400,h=300) ->
    if $(window).width() < 640
      w *= 0.6
      h *= 0.6
    pts = []
    x -= w*0.5
    y -= h*0.5
    pts.push new paper.Point(x,y)
    pts.push new paper.Point(x+w,y)
    pts.push new paper.Point(x+w,y+h)
    pts.push new paper.Point(x,y+h)
    pts.push new paper.Point(x,y)
    # subdiv = GeomUtils.getSubdivPts pts, 20
    return pts

module.exports = GeomUtils