class AnimationUtils

  @scaleIn = (item, duration=1000) ->
    
    item.transformContent = false
    item.scaling = new paper.Point 0.1, 0.1
    s = item.scaling.clone()
    
    new TWEEN.Tween s
     .to { x:1, y:1 }, duration
     .easing TWEEN.Easing.Quadratic.Out
     .onUpdate (e) => item.scaling = s
     .start()


module.exports = AnimationUtils
    