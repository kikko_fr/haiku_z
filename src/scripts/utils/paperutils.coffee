class PaperUtils

  setButton : (item) ->
    item.onMouseEnter = =>
      $('body').css('cursor','pointer')
    item.onMouseLeave = =>
      $('body').css('cursor','default')

  removeButton : (item) ->
    item.onMouseEnter = null
    item.onMouseLeave = null

module.exports = PaperUtils