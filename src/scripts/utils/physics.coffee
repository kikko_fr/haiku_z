# http://charly-studio.com/blog/html5-rope-simulation-verlet-integration-and-relaxation/


class Point
  
  constructor: (@x, @y, @isPinned = true) ->
    @init_x = @prev_x = x
    @init_y = @prev_y = y
    @length = 0

  distance : (pt) ->
    sqDist = (@x - pt.x) * (@x - pt.x) + (@y - pt.y) * (@y - pt.y)
    return Math.sqrt sqDist


class Verlet

  constructor: ->
    @items = []
    @relaxationIterations = 1
    @gravity              = 0.005

  add: (pt) ->
    if @items.length
      pt.length = 10#@items[@items.length-1].distance(pt)
    @items.push pt

  clear : ->
    @items.splice 0, @items.length

  update: (ellapsedTime) ->
    # Apply verlet integration
    for item in @items
      prev_x = item.x
      prev_y = item.y
      if !item.isPinned
        @applyUnitaryVerletIntegration item, ellapsedTime
      item.prev_x = prev_x
      item.prev_y = prev_y
    
    # Apply relaxation
    for it in [0...@relaxationIterations]
      for item,i in @items
        if !item.isPinned
          if i > 0
            previous = @items[i - 1]
            @applyUnitaryDistanceRelaxation item, previous
      for item,i in @items
        item = @items[@items.length - 1 - i]
        if !item.isPinned
          if i > 0
            next = @items[@items.length - i]
            @applyUnitaryDistanceRelaxation item, next
    return

  applyUnitaryVerletIntegration: (item, ellapsedTime) ->
    item.x = 2 * item.x - item.prev_x
    item.y = 2 * item.y - item.prev_y + @gravity * ellapsedTime

  applyUnitaryDistanceRelaxation: (item, from) ->
    dx = item.x - from.x
    dy = item.y - from.y
    dstFrom = Math.sqrt(dx * dx + dy * dy)
    if dstFrom > item.length and dstFrom != 0
      item.x -= (dstFrom - item.length) * (dx / dstFrom) * 0.5
      item.y -= (dstFrom - item.length) * (dy / dstFrom) * 0.5

module.exports = 
  Verlet : Verlet
  Point : Point