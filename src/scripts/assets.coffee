class Assets

  svg : {}

  lang : 'fr'

  color_bg    : null
  color_zip   : null
  color1      : null
  color2      : null

  colorsets : [
      color_bg    : '#51dad4'
      color1      : '#2b2561'
      color2      : '#fffaec'
      color_zip   : '#4b5d77'
    ,
      color_bg    : '#2b2561'
      color1      : '#51dad4'
      color2      : '#fffaec'
      color_zip   : '#4b5d77'
    ,
      color_bg    : '#005cff'
      color1      : '#00cfff'
      color2      : '#18263a'
      color_zip   : '#4b5d77'
    ,
      color_bg    : '#18263a'
      color1      : '#00cfff'
      color2      : '#005cff'
      color_zip   : '#4b5d77'
    ,
      color_bg    : '#18263a'
      color1      : '#fc4236'
      color2      : '#ce2c81'
      color_zip   : '#4b5d77'
    ,
      color_bg    : '#4b5d77'
      color1      : '#fc4236'
      color2      : '#ce2c81'
      color_zip   : '#18263a'
    ,
      color_bg    : '#51dad4'
      color1      : '#4b5d77'
      color2      : '#fc4236'
      color_zip   : '#ffffff'
    ,
      color_bg    : '#4b5d77'
      color1      : '#51dad4'
      color2      : '#fc4236'
      color_zip   : '#ffffff'
  ]

  trackNames : []
  tracks : []
  currTrack : null

  sfxNames :
    trace:  'Trace_2'
    zip:    'Zip_Seduction_Ouverture'
    zoom:   'Zoom_2'
    chute:  'Chute_3'

  trackNames : ['Seduction', 'Seduction', 'Lutte', 'Stabilisaton', 'Engagement', 'Ouverture']

  sfx : {}

  onLoadProgress : null

  load : (onLoadComplete) ->
    @loadSVG => onLoadComplete()
    @currZipSfx = new Howl {}
    for k,n of @sfxNames
      @sfx[k] = new Howl {}

  loadSVG : (callback) ->
    paper.project.importSVG 'assets/zipper.svg', (item) =>
      item.fillColor = new paper.Color Assets::color_zip
      Assets::svg['zipper'] = new paper.Symbol item
      item.remove()
      callback()

  loadMusic : (callback) ->
    @numTotalSounds = @trackNames.length + 4
    numTracksLoaded = 0
    for trackName in @trackNames
      urls = []
      urls.push 'assets/music/'+trackName+ext for ext in ['.mp3', '.ogg']
      sound = new Howl
        urls: urls
        volume: 0
        loop: true
        onload: =>
          numTracksLoaded++
          if @onLoadProgress
            @onLoadProgress numTracksLoaded / @numTotalSounds
          if numTracksLoaded == @trackNames.length
            t.play() for t in @tracks
            @loadSfx callback
      @tracks.push sound

  loadSfx : (callback) ->
    numSfx = 4
    numLoaded = 0
    for sfxName,fileName of @sfxNames
      urls = []
      urls.push 'assets/sfx/'+fileName+ext for ext in ['.mp3', '.ogg']
      sound = new Howl
        urls: urls
        onload: =>
          numLoaded++
          if @onLoadProgress
            @onLoadProgress (@trackNames.length+numLoaded) / @numTotalSounds
          if numLoaded == numSfx
            @currZipSfx = @sfx['zip']
            @currZipSfx.volume 0
            @currZipSfx.loop true
            @currZipSfx.play() if !(getParam 'mute')
            callback()
      sound.volume 0 if getParam 'mute'
      @sfx[sfxName] = sound

  setTrack : (num) ->
    return if getParam 'mute'
    if @currTrack
      return if @currTrack == @tracks[num]
      @currTrack.fade 1, 0, 3000
    @currTrack = @tracks[num]
    if @currTrack
      @currTrack.fade 0, 1, 3000

  newColorSet : ->
    color = Assets::colorsets.random()
    Assets::color_bg  = color.color_bg
    Assets::color_zip = color.color_zip
    Assets::color1    = color.color1
    Assets::color2    = color.color2
    Assets::svg['zipper'].definition.fillColor = color.color_zip

module.exports = Assets