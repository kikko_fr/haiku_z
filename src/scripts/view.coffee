class Renderer

  constructor : ->
    canvas = document.getElementById 'paperjs-canvas'
    paper.setup canvas
    # console.log 'PaperJS v' + paper.version

    @view = new paper.Layer()
    @view.pivot = new paper.Point 0,0
    @view.transformContent = false

module.exports = 
  Renderer : Renderer