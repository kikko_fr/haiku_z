view       = require './view'
Assets     = require './assets'
GeomUtils  = require './utils/geomutils'
IntroScene      = require './scenes/intro_scene'
MultipleScene   = require './scenes/multiple_scene'
LineScene       = require './scenes/line_scene'
ParallaxScene   = require './scenes/parallax_scene'
PetrushkaScene  = require './scenes/petrushka_scene'


class App

  constructor : ->
    @renderer = new view.Renderer

    cx = paper.view.bounds.width*0.5
    cy = paper.view.bounds.height*0.5

    @view = new paper.Group
    @view.transformContent = false
    @view.pivot = new paper.Point cx, cy
    @renderer.view.addChild @view

    @currSceneName = null

    paper.view.on 'resize', @resize

  start : ->
    # paper.view.element.style.backgroundColor = 'black'
    langParam = getParam 'lang'
    if !langParam
      if window.haiku_vars
        langParam = window.haiku_vars.lang
    Assets::lang = langParam || 'fr'
    Assets::load =>
      Assets::newColorSet()
      paper.view.element.style.backgroundColor = Assets::color_bg
      sceneParam = getParam 'scene'
      isIntro = sceneParam is undefined || sceneParam is 'intro'
      if isIntro
        @launchStartupScene()
      Assets::loadMusic =>
        # paper.view.element.style.backgroundColor = Assets::color_bg
        if !isIntro
          @launchStartupScene()

  launchStartupScene : ->
    # if window.bDebug
    # @setupMultiple()
    # @setupPetrushka()
    sceneName = getParam 'scene'
    if sceneName
      setupFunc = 'setup'+sceneName[0].toUpperCase()+sceneName.substring(1)
      if @[setupFunc]
        @[setupFunc]()
      else switch sceneName
        when 'intro' then @setupIntro true
        when 'seduction' then @setupMultiple()
        when 'lutte' then @setupLine()
        when 'stabilisation' then @setupPetrushka()
        when 'engagement' then @setupParallax()
        when 'ouverture' then @setupIntro false
        else throw setupFunc + ' scene not found'
    else @setupIntro true
    # else
    #   # Assets::newColorSet()
    #   @setupIntro true
    paper.view.on 'frame', @update

  update : (e) =>
    TWEEN.update()
    for s in [@introScene, @multipleScene, @lineScene, @petrushkaScene, @parallaxScene]
      s.update 16 if s
    return

  resize : (e) =>
    if @introScene
      @introScene.resize()
      return
    else
      sname = @currSceneName
      capname = sname[0].toUpperCase()+sname.substring(1)
      cleanFunc = @['on'+capname+'SceneComplete']
      cleanFunc false
      setupFunc = @['setup'+capname]
      # console.log @
      # console.log setupFunc
      setupFunc()
    


  # -- SCENE SETUP

  setupIntro : (bFastForward=false) =>
    console.log 'setup intro'
    @introScene = new IntroScene @onIntroSceneComplete, @onIntroSceneFinished
    @introScene.setup !bFastForward
    @introScene.fastForwardToStep2() if bFastForward
    @view.addChild @introScene.view
    @currSceneName = 'intro'


  setupMultiple : =>
    console.log 'setup multiple'
    @multipleScene = new MultipleScene @onMultipleSceneComplete
    @multipleScene.setup()
    @view.insertChild 0, @multipleScene.view
    @currSceneName = 'multiple'

  setupLine : (order=null, isIntroAnimated=false) =>
    console.log 'setup line'
    @lineScene = new LineScene @onLineSceneComplete, @onLineSceneReachesLast
    @lineScene.setup order, isIntroAnimated
    @view.addChild @lineScene.view
    @currSceneName = 'line'

  setupPetrushka : =>
    console.log 'setup petrushka'
    @petrushkaScene = new PetrushkaScene @onPetrushkaSceneComplete
    @petrushkaScene.setup()
    # console.log @
    @view.insertChild 0, @petrushkaScene.view
    @currSceneName = 'petrushka'

  setupParallax : =>
    console.log 'setup parallax'
    @parallaxScene = new ParallaxScene @onParallaxSceneComplete
    @parallaxScene.setup Assets::color_bg
    @view.insertChild 0, @parallaxScene.view
    @currSceneName = 'parallax'



  # -- SCENE EVENTS

  onIntroSceneComplete : (bSetupNext=true) =>
    if bSetupNext
      @setupMultiple()

  onIntroSceneFinished : =>
    @introScene.clean()
    @introScene = null

  onMultipleSceneComplete : (bSetupNext=true) =>
    @multipleScene.clean()
    @multipleScene = null
    if bSetupNext
      @setupLine()

  onLineSceneReachesLast : =>
    @setupPetrushka()

  onLineSceneComplete : (bSetupNext=true) =>
    @lineScene.clean()
    @lineScene = null

  onPetrushkaSceneComplete : (bSetupNext=true) =>
    @petrushkaScene.clean()
    @petrushkaScene = null
    if bSetupNext
      if window.isFirefox
        @setupIntro()
      else
        @setupParallax()

  onParallaxSceneComplete : (bSetupNext=true) =>
    @parallaxScene.clean()
    @parallaxScene = null
    if bSetupNext
      @setupIntro()

module.exports = App