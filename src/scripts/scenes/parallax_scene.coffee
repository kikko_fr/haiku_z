Assets    = require '../assets'
Zipper    = require '../zippers/free_zipper'
GeomUtils = require '../utils/geomutils'
Parallax  = require '../effects/parallax'

class ParallaxScene

  constructor : (@onComplete)->

    cx = paper.view.bounds.width*0.5
    cy = paper.view.bounds.height*0.5

    @view = new paper.Group
    @view.transformContent = false
    @view.pivot = new paper.Point cx, cy

    @numOpened = 0
    
    @parallax = new Parallax
    @parallax.clickCallback = => @zoomIn()
    @view.addChild @parallax.view

    c1 = new paper.Color Assets::color1
    c2 = new paper.Color Assets::color2

    @palettes = [
      [c1, c2]
      [c2, c1]
      [c1, c2, c1, c2, c1, c2, c1, c2, c1, c2]
    ]

  setup : (color) ->
    if color 
      Assets::setTrack 4
      
    if !color && @parallax.shapes.length
      # if @parallax.shapes.length
      color = @parallax.shapes[@parallax.shapes.length-1].fillColor
      # else
      #   rdmColor = @palettes[0][0]
      #   color = new paper.Color rdmColor

    color = new paper.Color color
    paper.view.element.style.backgroundColor = color.toCSS()

    @reset()

    @zipper = new Zipper color
    @zipper.onRelease = @zipperComplete
    @view.addChild @zipper.view
    @zipper.setup()

    if @numOpened == @palettes.length-1
      Assets::newColorSet()
      @palettes.last().push new paper.Color(Assets::color_bg)
    @setupShape()

  clean : ->
    @reset()
    @view.remove()
    @parallax = null
    paper.view.element.style.backgroundColor = new paper.Color(Assets::color_bg).toCSS()
    clearInterval @itvl

  reset : ->
    return if !@zipper
    @zipper.clear()
    @zipper.onRelease = null
    @zipper.view.remove()
    @parallax.clear()
    @view.scaling = new paper.Point 1,1

  zoomIn : ->
    @parallax.lock()
    @zipper.outSpeed = 1
    Assets::sfx['zoom'].play()
    itvl = setInterval =>
      @zipper.outSpeed *= 1.0003
      @zipper.handle.remove()
      speed = @zipper.outSpeed
      if @zipper.outSpeed >= 1.09
        clearInterval itvl
        if @numOpened >= @palettes.length
          @onComplete() if @onComplete
        else
          @setup()
      @view.scale speed, speed
    , 17


  zipperComplete : =>
    @parallax.animate()
    # @parallax.isAnimating = false
    @numOpened++


  setupShape : ->

    cx = paper.view.bounds.width*0.5
    cy = paper.view.bounds.height*0.5
    shapes = ['triangle', 'circle', 'poly', 'rect'] #, 
    type = shapes.random()

    pts = null
    subdivPts = null

    switch type
      when 'triangle'
        pts = GeomUtils.getPolyPts cx, cy, 200, 3
        subdivPts = GeomUtils.getSubdivPts pts, 20  
      when 'circle'
        radius = 100 + Math.random() * 200
        pts = GeomUtils.getCirclePts cx, cy, radius
        subdivPts = pts.slice 0, pts.length
      when 'poly'
        radius = 100 + Math.random() * 200
        steps = 3 + Math.floor( (Math.random()*5) )
        pts = GeomUtils.getPolyPts cx, cy, radius, steps
        subdivPts = GeomUtils.getSubdivPts pts, 10
      when 'star'
        innerRadius = 150 + Math.random() * 200
        outerRadius = innerRadius - 50 - (Math.random()) * 30
        steps = 7 + Math.floor( (Math.random()*8) )
        pts = GeomUtils.getStarPts cx, cy, innerRadius, outerRadius, steps
        subdivPts = GeomUtils.getSubdivPts pts, 5
      when 'rect'
        w = 200 + Math.random() * 200
        h = 200 + Math.random() * 200
        pts = GeomUtils.getRectPts cx, cy, w, h
        subdivPts = GeomUtils.getSubdivPts pts, 20

    @parallax.setup type, pts, @palettes[@numOpened], @numOpened==@palettes.length-1

    @parallax.view.visible = false

    Assets::sfx['trace'].play()

    # for pt in subdivPts
    num = 0
    @itvl = itvl = setInterval =>
      pt = subdivPts[num]
      @zipper.add pt.x, pt.y
      if ++num >= subdivPts.length
        clearInterval itvl
        @parallax.view.visible = true
    , 25

    # @parallax.setGradient c1, c2
    # @parallax.setPalette 
    

  update : (dt) =>
    @zipper.update dt
    @parallax.update dt



module.exports = ParallaxScene