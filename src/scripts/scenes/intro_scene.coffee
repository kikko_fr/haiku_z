Assets     = require '../assets'
Zipper     = require '../zippers/particles_zipper'
GeomUtils  = require '../utils/geomutils'
PaperUtils = require '../utils/paperutils'

class IntroScene

  constructor : (@onComplete, @onFinished) ->
    cx = paper.view.bounds.width*0.5
    cy = paper.view.bounds.height*0.5

    @view = new paper.Group
    @view.transformContent = false
    @view.pivot = new paper.Point cx, cy

    @isStep2 = false

    @numOpened = 0
    @zipper = null
    @radius = 0

  setup : (@doPlayIntro=false) ->

    # if @doPlayIntro
    #   Assets::setTrack 5
    #   Assets::sfx['trace'].play()
    # else
    #   Assets::setTrack 0

    pts = @getPts()
    color = new paper.Color Assets::color_bg
    @zipper = @addZipper pts, color, @doPlayIntro
    if @doPlayIntro
      @zipper.startParticles()

  resize : ->
    if @text
      @text.position = paper.view.center.clone()
    @view.position = paper.view.center.clone()
    if @isStep2
      @view.position.y += paper.view.bounds.height

  fastForwardToStep2 : ->
    @isStep2 = true
    midPoint = @zipper.baseline.segments[Math.floor(@zipper.baseline.segments.length*0.5)].point
    # @zipper.drag
    #   clientX : midPoint.x
    #   clientY : midPoint.y
    #   preventDefault : ->
    # , true
    sh = paper.view.bounds.height
    # setTimeout =>
    @zipper.startParticles true
    @zipper.source.maxAngle = 180
    @zipper.particles.setMinY -sh*1.5-10
    for p in @zipper.particles.particles
      p.view.position.y -= sh * 0.5
    # , 250
    @view.position.x = paper.view.bounds.width*0.5
    @view.position.y = sh*1.5
    # @showText 'Entre-' + number_trans('99').toLowerCase()

    Assets::onLoadProgress = (pct) =>
      pct = Math.round (1-pct) * 100
      if pct == 0
        text = $(".intro.lang-#{Assets::lang} p").html()
        bttext = $(".intro.lang-#{Assets::lang} a.explore").html()
        console.log text
        @showText text, bttext
      else
        text = pct + '%'
        @showText text

  clean : ->
    @zipper.clear()
    @zipper.onRelease = null
    @zipper.view.remove()
    @view.remove()
    @onComplete = null

  zipperComplete : =>
    setTimeout =>
      @slideDown()
      # Assets::setTrack 0
      # @fadeColor()
    , 2000
    # if ++@numOpened == @num
    #   setTimeout @onComplete, 2000

  slideDown : =>
    @isStep2 = true
    @zipper.particles.setMinY -paper.view.bounds.height*2-10
    dx = paper.view.bounds.width*0.5
    dy = paper.view.bounds.height*2
    t = new TWEEN.Tween @view.position
    .to {x:dx, y:dy}, 4000
    .easing TWEEN.Easing.Quadratic.InOut
    .onComplete => 
      window.haiku_vars.toggleMenu()
      @showText null, $(".intro.lang-#{Assets::lang} a.continue").html()
    .start()

  fadeColor : ->
    curr = new paper.Color paper.view.element.style.backgroundColor
    next = new paper.Color Assets::color_bg
    t = new TWEEN.Tween curr
    .to {red:next.red, green:next.green, blue:next.blue}, 1000
    .onUpdate -> paper.view.element.style.backgroundColor = curr.toCSS()
    .start()

  # showIntroText : ->
  #   @introText = new paper.Raster 'text-fr'
  #   @introText.position = paper.view.center
  #   @view.addChild @introText

  showText : (innerText, buttonText) =>
    cx = paper.view.bounds.width*0.5
    cy = paper.view.bounds.height*0.5

    if !@text

      @text = new paper.Group
      @text.opacity = 0
      @text.transformContent = false
      @text.position = paper.view.center
      
      if innerText
        @pointText = new paper.PointText 
          content : innerText || $('#title').html()
          fillColor : 'white'
          fontFamily : 'tstar_twregular'
          justification : 'center'
          fontSize : 24
        @text.addChild @pointText

      bt = @getButton()
      if @pointText
        bt.position.y = @pointText.bounds.height + 50
        bt.visible = false

      bt.position.x -= bt.bounds.width * 0.5
      @text.addChild bt

      t = new TWEEN.Tween @text
      .to {opacity:1}, 2000
      .start()

      PaperUtils::setButton @text
      @text.onMouseDown = => @exit()

    else if innerText

      @pointText.content = innerText
      @text.position.x = cx
      @text.position.y = cy
      @pointText.fillColor = 'white'
      bt = @getButton()
      bt.position.y = @pointText.bounds.height + 40
      if @doPlayIntro
        @zipper.source.maxAngle = Math.PI
        Assets::setTrack 5
      else
        Assets::setTrack 0

    if buttonText
      @text.position.y = cy - 40
      if buttonText=='Continuer'
        @buttonText.position.x -= 5
      @buttonBg.fillColor = Assets::color1
      @buttonText.content = buttonText
      @button.visible = true

  getButton : (text) ->
    if !@button
      @button = new paper.Group
      @button.transformContent = false
      @button.pivot = new paper.Point
      @buttonText = new paper.PointText
        content : 'Continue'
        fillColor : 'white'
        fontFamily : 'tstar_twregular'
        fontSize : 24
      @buttonText.transformContent = false
      @bgBds = @buttonText.bounds.clone()
      paddingX = 30
      paddingY = 12
      @bgBds.width += paddingX * 2
      @bgBds.height += paddingY * 2
      @buttonBg = new paper.Path.Rectangle @bgBds, @bgBds.height*0.5
      @buttonBg.fillColor = 'blue'
      @button.addChild @buttonBg
      @button.addChild @buttonText
      @buttonText.position.x += paddingX
      @buttonText.position.y += paddingY
    return @button


  exit : ->
    t = new TWEEN.Tween @text
    .to {opacity:0}, 1000
    .onComplete =>
      @onComplete()
      @text.remove()
    .start()

    @zipper.particles.stop()
    @zipper.particles.fadeOut => @onFinished()

  getPts : () ->
    type = ['circle', 'poly'].random() # 'rect', 'star'

    pts = null
    subdivPts = null

    cx = paper.view.bounds.width*0.5
    cy = paper.view.bounds.height*0.5

    switch type
      when 'circle'
        pts = GeomUtils.getCirclePts cx, cy, 200, 60, -Math.PI
        subdivPts = pts.slice 0, pts.length
      when 'poly'
        pts = GeomUtils.getPolyPts cx, cy, 200, 10, -Math.PI
        subdivPts = GeomUtils.getSubdivPts pts, 10
      when 'star'
        pts = GeomUtils.getStarPts cx, cy
        subdivPts = GeomUtils.getSubdivPts pts, 5
      when 'rect'
        pts = GeomUtils.getRectPts cx, cy
        subdivPts = GeomUtils.getSubdivPts pts, 20  

    return subdivPts


  addZipper : (pts, color, doPlayIntro=false) ->

    zipper = new Zipper color
    zipper.setup()
    zipper.mute = !doPlayIntro
    zipper.onRelease = @zipperComplete

    if doPlayIntro
      i = 0
      itvl = setInterval =>
        zipper.add pts[i].x, pts[i].y
        if ++i >= pts.length
          clearInterval itvl
      , 16
    else
      for i in [0...pts.length]
        zipper.add pts[i].x, pts[i].y

    @view.addChild zipper.view

    return zipper
    

  update : (dt) =>
    @zipper.update dt

module.exports = IntroScene