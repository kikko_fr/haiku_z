Assets    = require '../assets'
Zipper    = require '../zippers/unstich_zipper'
GeomUtils = require '../utils/geomutils'

class PetrushkaScene

  constructor : (@onComplete, @num = 4)->

    cx = paper.view.bounds.width*0.5
    cy = paper.view.bounds.height*0.5

    @view = new paper.Group
    @view.transformContent = false
    @view.pivot = new paper.Point cx, cy

    @numOpened = 0
    @zippers = []

    
  setup : ->
    Assets::setTrack 3

    pts = @getPts()
    cArr = [Assets::color1, Assets::color2]
    for i in [0...@num]
      color = new paper.Color cArr[i%2]
      @addZipper pts, color, 0.2+(i+1)/@num

  # resize : ->
  #   @view.position = paper.view.center

  clean : ->
    for z in @zippers
      z.clear()
      z.onRelease = null
      z.view.remove()
    @view.remove()
    console.log 'remove'
    @onComplete = null


  zipperComplete : =>
    if ++@numOpened == @num
      setTimeout @onComplete, 2000


  getPts : () ->
    type = ['circle', 'poly', 'triangle', 'rect'].random()

    pts = null
    subdivPts = null

    cx = paper.view.bounds.width*0.5
    cy = paper.view.bounds.height*0.5

    switch type
      when 'triangle'
        pts = GeomUtils.getPolyPts cx, cy, 200, 3
        subdivPts = GeomUtils.getSubdivPts pts, 20  
      when 'circle'
        pts = GeomUtils.getCirclePts cx, cy
        subdivPts = pts.slice 0, pts.length
      when 'poly'
        steps = 3 + Math.floor( (Math.random()*5) )
        pts = GeomUtils.getPolyPts cx, cy, 200, steps
        subdivPts = GeomUtils.getSubdivPts pts, 10
      when 'star'
        innerRadius = 200 + Math.random() * 150
        outerRadius = innerRadius - 50 - (Math.random()) * 30
        steps = 7 + Math.floor( (Math.random()*8) )
        pts = GeomUtils.getStarPts cx, cy, innerRadius, outerRadius, steps
        subdivPts = GeomUtils.getSubdivPts pts, 5
      when 'rect'
        pts = GeomUtils.getRectPts cx, cy
        subdivPts = GeomUtils.getSubdivPts pts, 20  

    return subdivPts

  addZipper : (pts, color, scale) ->

    zipper = new Zipper color
    zipper.setup()
    zipper.onRelease = @zipperComplete

    cx = paper.view.bounds.width*0.5 * (1-scale)
    cy = paper.view.bounds.height*0.5 * (1-scale)

    for i in [0...pts.length]
      zipper.add pts[i].x * scale + cx, pts[i].y * scale + cy

    @zippers.push zipper
    @view.addChild zipper.view
    

  update : (dt) =>
    z.update dt for z in @zippers
    return

module.exports = PetrushkaScene