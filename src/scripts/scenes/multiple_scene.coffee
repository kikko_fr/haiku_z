Assets    = require '../assets'
Zipper    = require '../zippers/unstich_zipper'
GeomUtils = require '../utils/geomutils'

class MultipleScene

  constructor : (@onComplete, @numX = 3, @numY = 2)->

    grid = [[2,2],[3,2]].random()
    @numX = grid[0]
    @numY = grid[1]
    if $(window).width() < 640
      @numX = 1
      @numY = 2

    cx = paper.view.bounds.width*0.5
    cy = paper.view.bounds.height*0.5

    @view = new paper.Group
    @view.transformContent = false
    @view.pivot = new paper.Point cx, cy

    @total = 0
    @numOpened = 0
    @zippers = []

    
  setup : ->
    Assets::setTrack 1

    pts = @getPts()
    marginX = 250
    marginY = 300
    marginY = 250 if $(window).width() < 640
    color = Assets::color_bg
    c = new paper.Point
      x : ((@numX-1) * marginX)
      y : ((@numY-1) * marginY)

    offsets = []
    num = 0
    for i in [0...@numY]
      for j in [0...@numX]
        offsets.push
          x : j * marginX - c.x * 0.5
          y : i * marginY - c.y * 0.5
          num : num++

    for i in [0...offsets.length]
      setTimeout (offset) =>
        @addZipper pts, color, offset
      , i*250, new paper.Point offsets[i]

    underZippers = []
    for col in [Assets::color1, Assets::color2]
      rdmPos = Math.floor(Math.random()*offsets.length)
      offset = offsets.splice(rdmPos, 1)[0]
      setTimeout (col, offset) =>
        z = @addZipper pts, col, offset, false
        z.refZipper = @zippers[offset.num]
        z.refZipper.underZipper = z
        z.onMouseDown = @onZipperDown
        z.onMouseUp = @onZipperUp
        z.onDrag = @onZipperDrag
        z.refZipper.onMouseDown = @onZipperDown
        z.refZipper.onMouseUp = @onZipperUp
        z.refZipper.onDrag = @onZipperDrag
        underZippers.push z
      , Math.max(@numY*@numX*500, 2000), col, offset

    setTimeout =>
      # move zippers under colored zippers to the back
      for uz in underZippers
        @view.insertChild uz.refZipper.view.index-1, uz.view
      # enable zippers
      zipper.enableHandle() for zipper in @zippers
      return
    , Math.max(@numY*@numX*500, 2000) + 250

    @total = @numX * @numY + 2
    

  # resize : ->
  #   @view.position = paper.view.center.clone()

  onZipperDown : (e, z) =>
    pairedZipper = @getPairedZipper z
    ev =
      clientX : e.clientX + (z.offset.x - pairedZipper.offset.x)
      clientY : e.clientY + (z.offset.y - pairedZipper.offset.y)
    pairedZipper.startDrag ev, true

  onZipperUp : (e, z) =>
    pairedZipper = @getPairedZipper z
    pairedZipper.isDragging = false

  onZipperDrag : (e, z) =>
    pairedZipper = @getPairedZipper z
    ev =
      clientX : e.clientX + (pairedZipper.offset.x - z.offset.x)
      clientY : e.clientY + (pairedZipper.offset.y - z.offset.y)

    pairedZipper.drag ev, true

  getPairedZipper : (z) ->
    if z.underZipper
      other = if z is @zippers.last().refZipper then @zippers[@zippers.length-2].refZipper else @zippers.last().refZipper
    else if z.refZipper
      other = if z is @zippers.last() then @zippers[@zippers.length-2] else @zippers.last()
    return other

  clean : ->
    for z in @zippers
      z.clear()
      z.onRelease = null
      z.view.remove()
    @view.remove()
    @onComplete = null


  zipperComplete : =>
    if ++@numOpened == @total
      setTimeout @onComplete, 2000


  getPts : () ->
    type = ['circle', 'poly', 'triangle', 'rect'].random() # 'rect', 'star'

    pts = null
    subdivPts = null

    cx = paper.view.bounds.width*0.5
    cy = paper.view.bounds.height*0.5

    switch type
      when 'triangle'
        pts = GeomUtils.getPolyPts cx, cy, 100, 3
        subdivPts = GeomUtils.getSubdivPts pts, 20  
      when 'circle'
        pts = GeomUtils.getCirclePts cx, cy, 100, 50, Math.PI*0.5
        subdivPts = pts.slice 0, pts.length
      when 'poly'
        steps = 3 + Math.floor( (Math.random()*5) )
        pts = GeomUtils.getPolyPts cx, cy, 100, steps
        subdivPts = GeomUtils.getSubdivPts pts, 8
      when 'star'
        pts = GeomUtils.getStarPts cx, cy
        subdivPts = GeomUtils.getSubdivPts pts, 5
      when 'rect'
        pts = GeomUtils.getRectPts cx, cy, 150, 170
        subdivPts = GeomUtils.getSubdivPts pts, 20  

    return subdivPts

  addZipper : (pts, color, offset, doPlayIntro=true) ->
    color = new paper.Color color
    zipper = new Zipper color
    zipper.offset = offset # for cloned opening only
    zipper.setup()
    zipper.disableHandle()
    zipper.onRelease = @zipperComplete

    if doPlayIntro
      if @zippers.length%2
        Assets::sfx['trace'].play()
      i = 0
      itvl = setInterval =>
        zipper.add pts[i].x + offset.x, pts[i].y + offset.y
        if ++i >= pts.length
          clearInterval itvl
      , 15
    else
      for i in [0...pts.length]
        zipper.add pts[i].x + offset.x, pts[i].y + offset.y

    @zippers.push zipper
    @view.insertChild 0, zipper.view
    return zipper
    

  update : (dt) =>
    for z in @zippers
      z.update dt
    return

module.exports = MultipleScene