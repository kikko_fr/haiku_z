Assets = require '../assets'
Zipper = require '../zippers/line_zipper'

class LineScene

  constructor : (@onComplete, @onReachLast) ->

  setup : (@order=null, isIntroAnimated=false) ->
    Assets::setTrack 2

    @zippers = []
    @activeZipper = null
    @totalZippers = 0
    @numZippers = 4

    @view = new paper.Group
    
    c1 = Assets::color1
    c2 = Assets::color2

    if !@order
      @order = [
        [
          [false, false]
          [true, false]
          [false, true]
          [true, true]
        ], [
          [false, true]
          [true, true]
          [false, false]
          [true, false]
        ], [
          [true, true]
          [false, true]
          [true, false]
          [false, false]
        ], [
          [true, false]
          [false, false]
          [true, true]
          [false, true]
        ]
      ].random()
    zipper = @addZipper Assets::color_bg, Assets::color_bg, @order[0][0], @order[0][1], isIntroAnimated
    zipper.onIntroFinished = =>
      @addZipper c1, c2, @order[1][0], @order[1][1]
      @addZipper c1, c2, @order[2][0], @order[2][1]
      @addZipper c2, c1, @order[3][0], @order[3][1]
      @activeZipper = @zippers[0]
      @activeZipper.setupControls()

    window.addEventListener 'resize', @onResize

  resize : ->
    

  clean : ->
    while @zippers.length
      zipper = @zippers[0]
      zipper.clean()
      @zippers.shift()
    totalZippers = 0
    @view.remove()
    window.removeEventListener 'resize', @onResize

  addZipper : (c1, c2, isVertical=false, isReversed=false, isIntroAnimated=false) ->
    Assets::sfx['trace'].play()
    isIntroAnimated = isIntroAnimated || @zippers.length == 0
    zipper = new Zipper c1, c2, isVertical, isReversed, isIntroAnimated
    zipper.onAutoFinish = @onAutoFinish
    zipper.onComplete = @onZipperOpened
    @zippers.push zipper
    @view.insertChild 0, zipper.view
    return zipper

  onZipperOpened : (zipper) =>
    @zippers.splice @zippers.indexOf(zipper), 1
    @totalZippers++

    if @totalZippers == @numZippers-1
      @onReachLast()
    else if @totalZippers > @numZippers-1
      @onComplete @
      return

    @activeZipper = @zippers[0]
    @activeZipper.setupControls()


  update : (dt) =>
    @activeZipper.update dt if @activeZipper


module.exports = LineScene