view    = require './view'
Zipper = require '../zippers/free_zipper'


class Editor
  
  constructor : (@zipper) ->
    @isEditing = false

  start : ->
    paper.view.element.addEventListener 'mousedown', @mouseDownView
    paper.view.element.addEventListener 'mouseup', @mouseUpView
    paper.view.element.addEventListener 'touchstart', @touchStart
    paper.view.element.addEventListener 'touchend', @touchEnd

  stop : ->
    paper.view.element.removeEventListener 'mousedown', @mouseDownView
    paper.view.element.removeEventListener 'mouseup', @mouseUpView
    paper.view.element.removeEventListener 'touchstart', @touchStart
    paper.view.element.removeEventListener 'touchend', @touchEnd

  mouseDownView : (e) =>
    if !@zipper.isDragging && !@isEditing
      @startEdit()
      window.addEventListener 'mousemove', @drag
      @add e
      
  startEdit : ->
    @zipper.clear()
    @isEditing = true

  mouseUpView : (e) =>
    if @isEditing
      window.removeEventListener 'mousemove', @drag
      @isEditing = false

  drag : (e) =>
    @add e

  touchStart : (e) =>
    if !@zipper.isDragging && !@isEditing
      @startEdit()
      paper.view.element.addEventListener 'touchmove', @touchMove
      e.clientX = e.touches[0].clientX
      e.clientY = e.touches[0].clientY
      @add e

  touchEnd : (e) =>
    if @isEditing
      paper.view.element.removeEventListener 'touchmove', @touchMove
      @isEditing = false


  touchMove : (e) =>
    e.clientX = e.touches[0].clientX
    e.clientY = e.touches[0].clientY
    @add e

  add : (e) =>
    x = e.clientX
    y = e.clientY
    p = new paper.Point x,y
    last = @zipper.pts[@zipper.pts.length-1]
    if last
      dist = p.getDistance last.vpt
    if @zipper.pts.length is 0 || dist > 10
      @zipper.add x,y


module.exports = Editor