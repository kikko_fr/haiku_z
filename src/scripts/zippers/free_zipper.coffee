Assets     = require '../assets'
AnimUtils  = require '../utils/animutils'
physics    = require '../utils/physics'
PaperUtils = require '../utils/paperutils'

class FreeZipper

  constructor : (color, @isIntroAnimated=false) ->

    @pts = []

    @verlet   = new physics.Verlet
    @handle = null
    @handleDst = null
    @handleDstAngle = 0

    @isDragging = false
    @isReleased = false
    @isOutOfScreen = false

    @clientX = @clientY = 0

    @view = new paper.Group()

    @bg = new paper.Path
      fillColor: color
    @view.addChild @bg

    @baseline = new paper.Path
      strokeColor: Assets::color_zip,
      strokeWidth: 8,
      dashArray: [2, 5]
    @view.addChild @baseline

    @pinline = new paper.Path
      strokeColor: Assets::color_zip,
      strokeWidth: 8,
      dashArray: [2, 5],
    @view.addChild @pinline

    backColor = color.clone()
    backColor.brightness *= 0.8
    @back = new paper.Path
      fillColor: backColor
    @view.addChild @back

    @unpinline = new paper.Path
      strokeColor: Assets::color_zip,
      strokeWidth: 8,
      dashArray: [2, 5],
      strokeJoin : 'round'
    @view.addChild @unpinline


  setup : ->

    @setupHandle()

  add : (x, y) ->

    # unlock previous point if it's not the first one
    if @pts.length>1
      @pts[@pts.length-1].isLocked = false

    p = new paper.Point x, y
    pt =
      isLocked : true
      vpt : p.clone()
      ppt : new physics.Point x, y, true

    # @pinline.add pt.vpt
    @verlet.add pt.ppt
    @pts.push pt

    @bg.add p.clone()
    # @bg.insert 0, p.clone()

    @baseline.insert 0, p.clone()

    # if added first point, set handle position & back point
    if @pts.length is 1 && @handle
      @handleDst = new paper.Point @pts[0].ppt
      @handle.position = @handleDst.clone()
      @handle.visible = true
      @addBackPt pt
      @unpinline.add pt.vpt.clone()
      @unpinline.segments[@unpinline.segments.length-1].pt = pt


  clear : ->

    @pinline.removeSegments()
    @unpinline.removeSegments()
    @baseline.removeSegments()
    @back.removeSegments()
    @verlet.clear()
    @bg.clear()
    @pts.splice 0, @pts.length

    # c.remove() for c in @handle.children
    @handle.remove()# = false


  setupHandle : ->

    @handle = new paper.Group
    @handle.transformContent = false
    AnimUtils.scaleIn @handle, 500

    @handleAngle = 0
    bg = new paper.Path.Circle
      fillColor : 'white'
      opacity : 0
      radius : 100
    @handle.addChild bg
    zip = Assets::svg['zipper'].place()
    zip.position.x = zip.bounds.width * 0.4
    @handle.addChild zip
    
    @view.addChild @handle

    PaperUtils::setButton @handle
    @handle.on 'mousedown', @mouseDownView

  disableHandle : ->
    PaperUtils::removeButton @handle
    @handle.off 'mousedown', @mouseDownView
    paper.view.element.removeEventListener 'mouseup', @mouseUpView
    paper.view.element.removeEventListener 'touchend', @mouseUpView

  enableHandle : ->
    PaperUtils::setButton @handle
    @handle.on 'mousedown', @mouseDownView

  touchMove : (e) =>
    e.clientX = e.touches[0].clientX
    e.clientY = e.touches[0].clientY
    @drag e

  mouseDownView : (e) =>

    if e.touches
      e.clientX = e.touches[0].clientX
      e.clientY = e.touches[0].clientY
    @startDrag e
    window.addEventListener 'mousemove', @drag
    window.addEventListener 'touchmove', @touchMove
    paper.view.element.addEventListener 'mouseup', @mouseUpView
    paper.view.element.addEventListener 'touchend', @mouseUpView
    @onMouseDown(e, @) if @onMouseDown


  mouseUpView : (e) =>

    @isDragging = false
    window.removeEventListener 'mousemove', @drag
    window.removeEventListener 'touchmove', @touchMove
    paper.view.element.removeEventListener 'mouseup', @mouseUpView
    paper.view.element.removeEventListener 'touchend', @mouseUpView
    @onMouseUp(e, @) if @onMouseUp


  startDrag : (e) ->
    
    e.preventDefault() if e.preventDefault
    e.stopPropagation() if e.stopPropagation

    @handleOffset = 
      x : e.clientX - @handle.position.x
      y : e.clientY - @handle.position.y
    @isDragging = true


  drag : (e) =>

    e.preventDefault() if e.preventDefault
    closest = @getClosestPt {x:e.clientX, y:e.clientY}
    return if closest is -1 || Math.abs(closest-@unpinline.segments.length) > 20
    ppt = @pts[closest].ppt
    @handleDst.x = ppt.init_x
    @handleDst.y = ppt.init_y


  getClosestPt : (from) ->

    closest = -1
    minDist = 99999
    for i in [0...@pts.length]
      pt = @pts[i].ppt
      dstSq = (from.x-pt.init_x) * (from.x-pt.init_x) + (from.y-pt.init_y) * (from.y-pt.init_y)
      if dstSq < minDist
        minDist = dstSq
        closest = i
    return closest
    

  updatePinning : ->
    
    if @isReleased
      # @pts[0].ppt.y = @pts[@pts.length-1].ppt.y
      return

    closest = @getClosestPt @handle.position

    for i in [0..closest]
      pt = @pts[i]
      if pt.ppt.isPinned
        @unpin pt
    for i in [closest+1...@pts.length]
      pt = @pts[i]
      if !pt.ppt.isPinned
        @pin pt
    return


  unpin : (pt) ->

    if !pt.isLocked
      pt.ppt.isPinned = false
      @addBackPt pt
      # i = @pts.indexOf pt
      # @pinline.removeSegment 

      @pinline.removeSegment @pinline.segments.length-1
      @unpinline.add pt.vpt.clone()
      @unpinline.segments[@unpinline.segments.length-1].pt = pt

      # console.log @unpinline.segments.length + ' ' + @pts.length
      tolerance = 4
      if !@isReleased && @unpinline.segments.length >= @pts.length-tolerance-1
        @release()


  pin : (pt) ->
    return if @isReleased

    if !pt.isLocked
      pt.ppt.x = pt.ppt.init_x
      pt.ppt.y = pt.ppt.init_y
      pt.ppt.isPinned = true
      @back.removeSegment @back.segments.length-1

      @unpinline.removeSegment @unpinline.segments.length-1
      @pinline.add pt.vpt.clone()
      @pinline.segments[@pinline.segments.length-1].pt = pt


  release : ->
    Assets::currZipSfx.volume 0
    # Assets::sfx['chute'].play()

    @isReleased = true
    for pt,i in @pts
      pt.isLocked = false
      if pt.ppt.isPinned
        @unpin pt

    @pts[@pts.length-1].ppt.isPinned = false

    @mouseUpView()
    @disableHandle()

    setTimeout =>
      @handle.visible = false
      @isOutOfScreen = true
      @clear()
      @unpinline.remove()
      @bg.remove()
      @back.remove()
    , 5000

    if @onRelease
      @onRelease()


  addBackPt : (pt) ->
    @back.add pt.vpt.clone()
    @back.segments[@back.segments.length-1].pt = pt


  update : (dt) ->

    return if (!@pts.length || @isOutOfScreen)

    sx = @handle.position.x
    sy = @handle.position.y

    dsx = (@handleDst.x - sx) * 0.1
    dsy = (@handleDst.y - sy) * 0.1

    if @isDragging
      if Math.abs(dsx) + Math.abs(dsy) > 1
        @handleDstAngle = Math.atan2(dsy, dsx) / Math.PI * 180 
        @handleDstAngle += 360 if @handleDstAngle < 0
        @handleAngle += (@handleDstAngle-@handleAngle) * 0.2
    else
      @handleDstAngle = 90
      @handleAngle += (@handleDstAngle-@handleAngle) * 0.05
    @handle.rotation = @handleAngle


    sx += dsx
    sy += dsy

    @handle.position.x = sx
    @handle.position.y = sy

    if Math.abs(dsx) > 0.1 || Math.abs(dsy) > 0.1
      vol = Math.min(Math.max(0, Math.abs(dsx)+Math.abs(dsy) / 5), 0.7)
      Assets::currZipSfx.volume vol
      @updatePinning()
    else Assets::currZipSfx.volume (Assets::currZipSfx.volume()*0.9)

    if @isReleased
      @verlet.items[ @verlet.items.length-1 ].x = @verlet.items[0].x
      @verlet.items[ @verlet.items.length-1 ].y = @verlet.items[0].y
      @handle.position.x = @handleDst.x = @verlet.items[0].x
      @handle.position.y = @handleDst.y = @verlet.items[0].y

    @verlet.update 16

    for s in @unpinline.segments
      s.point.x = s.pt.ppt.x
      s.point.y = s.pt.ppt.y

    for pt,i in @pts
      @bg.segments[@pts.length-1-i].point.x = pt.ppt.x
      @bg.segments[@pts.length-1-i].point.y = pt.ppt.y

    for s in @back.segments
      s.point.x = s.pt.ppt.x
      s.point.y = s.pt.ppt.y
    return


module.exports = FreeZipper
