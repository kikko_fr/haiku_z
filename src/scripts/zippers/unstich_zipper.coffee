FreeZipper = require './free_zipper'

class UnstichZipper extends FreeZipper

  constructor : (color) ->
    super color

  unpin : (pt) ->
    super pt
    if !pt.isLocked
      @baseline.removeSegment @baseline.segments.length-1

  pin : (pt) ->
    return

  drag : (e, noCallback=false) =>

    e.preventDefault() if e.preventDefault

    if !noCallback
      @onDrag(e, @) if @onDrag

    closest = @getClosestPt {x:e.clientX, y:e.clientY}
    return if closest < @unpinline.segments.length-1 || Math.abs(closest-@unpinline.segments.length) > 20
      
    # 
    # closest = Math.min(closest, @unpinline.segments.length+1)
    # console.log closest
    # console.log noCallback

    ppt = @pts[closest].ppt
    @handleDst.x = ppt.init_x
    @handleDst.y = ppt.init_y

  # update : (dt) ->
  #   super dt
  #   if @isReleased
  #     @handle.position.x = @handleDst.x = @verlet.items[0].x
  #     @handle.position.y = @handleDst.y = @verlet.items[0].y

module.exports = UnstichZipper
