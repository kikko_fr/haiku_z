Assets     = require '../assets'
FreeZipper = require './free_zipper'
ParticlesSystem = require '../effects/particles'

class ParticlesZipper extends FreeZipper

  constructor : (color) ->
    super color

    @view.transformContent = false

    @source =
      type : 'circle'
      radius : 180
      x : paper.view.bounds.width*0.5
      y : paper.view.bounds.height*0.5
      maxAngle : 0

    @source.radius *= 0.6 if $(window).width() < 640

    @view.insertChild 0, @baseline
    @view.addChild @pinline
  

  startParticles : (@forceMaxAngle) ->
    @particles = new ParticlesSystem
    @particles.setup @source
    if @forceMaxAngle
      @source.maxAngle = Math.PI
      itvl = setInterval =>
        if @particles.isEmitting
          @particles.add()
          if @particles.length >= 20
            clearInterval itvl
        else clearInterval itvl
      , 200
    # @view.addChild @particles.view
    @view.insertChild 1, @particles.view

  pin : (pt) ->
    return

  unpin : (pt) ->
    super pt
    numUnpinned = @unpinline.segments.length * 0.3
    if @particles && @particles.length < numUnpinned && numUnpinned > 5
      for i in [@particles.length...numUnpinned]
        @particles.add()

  drag : (e, noCheck=false) =>
    e.preventDefault()
    closest = @getClosestPt {x:e.clientX, y:e.clientY}
    if !noCheck
      return if closest < @unpinline.segments.length-1
      return if closest > @unpinline.segments.length+10
    ppt = @pts[closest].ppt
    @handleDst.x = ppt.init_x
    @handleDst.y = ppt.init_y

  update : (dt) ->
    super dt

    if @mute
      Assets::currZipSfx.volume 0

    if @particles
      @particles.update dt if @particles

    return if @forceMaxAngle || !@handleDst
    
    cx = paper.view.bounds.width*0.5
    cy = paper.view.bounds.height*0.5
    a = Math.atan2 cy-@handleDst.y, cx-@handleDst.x
    @source.maxAngle = a if (a < Math.PI && a > 0 && Math.abs(a)>0.01)


module.exports = ParticlesZipper
