Assets     = require '../assets'
physics    = require '../utils/physics'
AnimUtils  = require '../utils/animutils'
PaperUtils = require '../utils/paperutils'

class LineZipper

  NUM_POINTS : 30

  constructor : (@color, @color2, @isVertical, @isReversed=false, @isIntroAnimated=false) ->

    @curr = -1

    @handle = null
    @handleDst = new paper.Point

    @isDragging = false
    @isFinishing = false

    @orientation = new paper.Point
    @basePos = new paper.Point

    @view = new paper.Group

    @l1bg = new paper.Path
      fillColor : @color
    @view.addChild @l1bg

    @l2bg = new paper.Path
      fillColor : @color2
    @view.addChild @l2bg

    @l1 = new paper.Path
      strokeColor: Assets::color_zip,
      strokeWidth: 8,
      dashArray: [2, 3]
    @view.addChild @l1

    @l2 = new paper.Path
      strokeColor: Assets::color_zip,
      strokeWidth: 8,
      dashArray: [2, 3]
    @view.addChild @l2

    @setupHandle()

    if @isVertical
      @setVertical()
    else 
      @setHorizontal()
    
    

  setHorizontal : ->
    
    sw = paper.view.bounds.width
    sh = paper.view.bounds.height

    if @isReversed
      @l1bg.add 0, sh
      @l1bg.add sw, sh
      @l2bg.add 0, 0
      @l2bg.add sw, 0
    else
      @l1bg.add sw, sh
      @l1bg.add 0, sh
      @l2bg.add sw, 0
      @l2bg.add 0, 0

    margin = sw / (LineZipper::NUM_POINTS-1)
    y = sh * 0.5
    if @isIntroAnimated
      i = 0
      itvl = setInterval =>
        if @isReversed
          @add (LineZipper::NUM_POINTS-i-1)*margin, y
        else
          @add i*margin, y
        if ++i >= LineZipper::NUM_POINTS
          clearInterval itvl
          @onIntroFinished() if @onIntroFinished
      , 25
    else 
      for i in [0...LineZipper::NUM_POINTS]
        if @isReversed
          @add (LineZipper::NUM_POINTS-i-1)*margin, y
        else
          @add i*margin, y

    @basePos.x = @handle.position.x = @handleDst.x = if @isReversed then sw-30 else 30
    @basePos.y = @handle.position.y = @handleDst.y = y

    @orientation.x = 1
    @orientation.y = 0

    @isVertical = false


  setVertical : ->
    
    sw = paper.view.bounds.width
    sh = paper.view.bounds.height

    if @isReversed
      @l1bg.add sw, 0
      @l1bg.add sw, sh
      @l2bg.add 0, 0
      @l2bg.add 0, sh
    else
      @l1bg.add sw, sh
      @l1bg.add sw, 0
      @l2bg.add 0, sh
      @l2bg.add 0, 0

    margin = sh / (LineZipper::NUM_POINTS-1)
    x = sw * 0.5
    if @isIntroAnimated
      i = 0
      itvl = setInterval =>
        if @isReversed
          @add x, (LineZipper::NUM_POINTS-i-1)*margin
        else
          @add x, i*margin
        if ++i >= LineZipper::NUM_POINTS
          clearInterval itvl
          @onIntroFinished() if @onIntroFinished
      , 25
    else
      for i in [0...LineZipper::NUM_POINTS]
        if @isReversed
          @add x, (LineZipper::NUM_POINTS-i-1)*margin
        else
          @add x, i*margin

    @basePos.x = @handle.position.x = @handleDst.x = x
    @basePos.y = @handle.position.y = @handleDst.y = if @isReversed then sh-30 else 30

    @handle.rotate 90

    @orientation.x = 0
    @orientation.y = 1

    @isVertical = true


  setupHandle : ->

    @handle = new paper.Group
    AnimUtils.scaleIn @handle, 500
    bg = new paper.Path.Circle
      fillColor : 'white'
      opacity : 0
      radius : 100
    @handle.addChild bg
    zip = Assets::svg['zipper'].place()
    if @isReversed
      zip.position.x = -zip.bounds.width * 0.25
      zip.rotate 180
    else
      zip.position.x = zip.bounds.width * 0.25
    @handle.addChild zip
    @view.addChild @handle
    PaperUtils::setButton @handle


  add : (x, y) ->

    @l1bg.add x, y
    @l2bg.add x, y

    pt = @l1.add x, y
    pt.init_x = x
    pt.init_y = y

    pt = @l2.add x, y
    pt.init_x = x
    pt.init_y = y


  clear : ->
    @l1bg.removeSegments()
    @l1.removeSegments()

    @l2bg.removeSegments()
    @l2.removeSegments()

  clean : ->
    @clear()
    @view.removeChildren()
    @view.remove()


  autoFinish : ->
    @removeControls()
    @isFinishing = true
    if @onAutoFinish
      @onAutoFinish this


  getClosestPt : (line, from) ->
    closest = -1
    minDist = 99999
    for i in [0...line.length]
      pt = line[i]
      dstSq = (from.x-pt.init_x) * (from.x-pt.init_x) + (from.y-pt.init_y) * (from.y-pt.init_y)
      if dstSq < minDist
        minDist = dstSq
        closest = i
    return closest
    

  update : (dt) ->

    return if @isIntroPlaying

    sw = paper.view.bounds.width
    sh = paper.view.bounds.height

    dst = Math.abs(@handleDst.x - @handle.position.x) + Math.abs(@handleDst.y - @handle.position.y)
    if dst > 0.1
      vol = Math.min(Math.max(0, dst / 3), 0.7)
      Assets::currZipSfx.volume vol
    else Assets::currZipSfx.volume (Assets::currZipSfx.volume()*0.9)

    bReadyForFinish = (@isVertical && @l1.segments[0].point.x > sw)
    bReadyForFinish = bReadyForFinish || (!@isVertical && @l1.segments[0].point.y > sh)
    if @isFinishing && bReadyForFinish 
      if @isVertical
        if @isReversed
          @speed = (@speed || 5) * 1.05
          @view.position.y -= @speed
          if @view.position.y < -sh * 1.7 && @onComplete
            @clean()
            @onComplete this
        else
          @view.position.y *= 1.025
          if @view.position.y > sh * 1.7 && @onComplete
            @clean()
            @onComplete this
      else
        if @isReversed
          @speed = (@speed || 5) * 1.05
          @view.position.x -= @speed
          if @view.position.x < -sw * 1.7 && @onComplete
            @clean()
            @onComplete this
        else
          @view.position.x *= 1.025
          if @view.position.x > sw * 1.7 && @onComplete
            @clean()
            @onComplete this
    else
      @handle.position.x = @handleDst.x
      @handle.position.y = @handleDst.y
      @updatePinning @l1
      @updatePinning @l2, -1

    for i in [0...@l1.segments.length]
      @l1bg.segments[i+2].point.x = @l1.segments[i].point.x
      @l1bg.segments[i+2].point.y = @l1.segments[i].point.y

    for i in [0...@l2.segments.length]
      @l2bg.segments[i+2].point.x = @l2.segments[i].point.x
      @l2bg.segments[i+2].point.y = @l2.segments[i].point.y
    

  updatePinning : (line, dir=1) ->

    prev = @curr
    @curr = @getClosestPt line.segments, @handle.position
    # if prev < @curr
    #   Assets::currZipSfx.rate = 1
    # else if prev > @curr
    #   Assets::currZipSfx.rate = -1
    if @curr == -1
      console.log "couldn't find"
      return
    sw = paper.view.bounds.width
    sh = paper.view.bounds.height

    if !@isVertical
      for i in [0...@curr]
        l = line.segments[i]
        pt = l.point
        dy = @basePos.y + sh * (@curr-i) / LineZipper::NUM_POINTS * dir
        speed = if dy*dir < pt.y*dir then 1 else 0.03
        pt.y += (dy - pt.y) * speed
    else
      for i in [0...@curr]
        l = line.segments[i]
        pt = l.point
        dx = @basePos.x + sw * (@curr-i) / LineZipper::NUM_POINTS * dir
        speed = if dx*dir < pt.x*dir then 1 else 0.03
        pt.x += (dx - pt.x) * speed

    for i in [@curr...line.segments.length]
      line.segments[i].point.y = line.segments[i].init_y
      line.segments[i].point.x = line.segments[i].init_x




  ### CONTROLS ###


  setupControls : ->

    @handle.on 'mousedown', @mouseDownView
    window.addEventListener 'mouseup', @mouseUpView
    window.addEventListener 'touchend', @mouseUpView

  removeControls : ->

    @handle.off 'mousedown', @mouseDownView
    window.removeEventListener 'mouseup', @mouseUpView
    window.removeEventListener 'touchend', @mouseUpView

  mouseDownView : (e) =>

    if e.touches
      e.clientX = e.touches[0].clientX
      e.clientY = e.touches[0].clientY
    @startDrag e
    window.addEventListener 'mousemove', @drag
    window.addEventListener 'touchmove', @touchMove


  mouseUpView : (e) =>

    @isDragging = false
    window.removeEventListener 'mousemove', @drag
    window.removeEventListener 'touchmove', @touchMove
    if @curr >= LineZipper::NUM_POINTS*0.75
      @autoFinish()


  startDrag : (e) ->
    
    e.preventDefault()
    e.stopPropagation()

    @handleOffset = 
      x : e.clientX - @handle.position.x
      y : e.clientY - @handle.position.y
    @isDragging = true


  touchMove : (e) =>

    e.clientX = e.touches[0].clientX
    e.clientY = e.touches[0].clientY
    @drag e

  drag : (e) =>

    e.preventDefault()

    sw = paper.view.bounds.width
    sh = paper.view.bounds.height
    x = e.clientX
    y = e.clientY
    if Math.abs(@orientation.x) > 0
      @handleDst.x = Math.min Math.max(0, x), sw
    if Math.abs(@orientation.y) > 0
      @handleDst.y = Math.min Math.max(0, y), sh


###  ###



module.exports = LineZipper