Assets = require '../Assets'

class ParticleP
  constructor : (@view) ->
    @velocity = new paper.Point
    @scaling = new paper.Point 0.1, 0.1
    @reset()

  reset : ->
    @scaling = new paper.Point 0.1,0.1
    @view.scaling = @scaling

  update : (dt) ->
    @scaling.x += (1 - @scaling.x) * 0.05
    @scaling.y += (1 - @scaling.y) * 0.05
    @view.scaling = @scaling
    @view.position.x += @velocity.x
    @view.position.y += @velocity.y


class ParticleSystem

  constructor : ->

    @view = new paper.Group
    @view.transformContent = false
    @particles = []
    @forces = []
    @symbols = []
    @length = 0
    @isEmitting = true

    for color in [Assets::color1, Assets::color2]
      geom = new paper.Path.Circle
        radius : 20
        fillColor : color
      symbol = new paper.Symbol geom
      @symbols.push symbol


  getRandomPosInSource : ->
    p = new paper.Point
    switch @source.type
      when 'circle'
        a = Math.PI - Math.random()*@source.maxAngle
        r = @source.radius
        p.x = @source.x + Math.cos(a) * r
        p.y = @source.y + Math.sin(a) * r
    return p

  setup : (@source) ->
    @minY = -10
      
  setMinY : (@minY) -> #

  add : ->
    v = @symbols.random().place @getRandomPosInSource()
    p = new ParticleP v
    p.velocity.x = Math.random() * 1 - 0.5
    p.velocity.y = -( 2 + Math.random() * 5)
    @view.addChild p.view
    @particles.push p
    @length++

  stop : ->
    cy = paper.view.bounds.height
    @isEmitting = false
    for p in @particles
      # console.log cy + ' ' + p.view.position.y
      if p.view.position.y > 10
        p.view.position.y = - cy * 2
        p.view.remove()
        p.view.visible = false


  fadeOut : (callback) ->
    cy = paper.view.bounds.height
    # console.log @particles
    for p in @particles
      if p.view.position.y < 10 && p.view.position.y > - cy
        t = new TWEEN.Tween p.view
        .to {opacity:0}, 1000
        .delay 6000
        .start()
    setTimeout callback, 7000

  edgeLoop : ->
    cx = paper.view.bounds.width
    cy = paper.view.bounds.height
    for p in @particles

      bds = p.view.bounds
      pos = p.view.position
      
      if pos.x < -bds.width * 0.5
        pos.x = cx + bds.width * 0.5
      else if pos.x > cx + bds.width * 0.5
        pos.x = -bds.width*0.5

      if pos.y < -bds.height * 0.5
        pos.y = cy + bds.height * 0.5
      else if pos.y > cy + bds.height * 0.5
        pos.y = -bds.height*0.5

  topReset : ->
    cy = paper.view.bounds.height
    for p in @particles
      pos = p.view.position
      if pos.y < @minY
        p.reset()
        np = @getRandomPosInSource()
        p.view.position.x = np.x
        p.view.position.y = np.y

  update : (dt) ->
    for f in @forces
      for p in @particles
        p.view.position.x += f.x
        p.view.position.y += f.y

    for p in @particles
      p.update dt

    @topReset() if @isEmitting
      



module.exports = ParticleSystem