PaperUtils = require '../utils/paperutils'

class Parallax

  constructor : ->

    @view = new paper.Group
    @view.transformContent = false
    @view.pivot = new paper.Point()
    @shapes = []
    @mask = null
    @offset = new paper.Point
    @clickCallback = null
    @amplitude = 0.3

    @currHighlighted = 0
    @isAnimating = false

    @initCenter = paper.view.center.clone()

    @isLocked = false

    PaperUtils::setButton @view
    @view.onMouseDown = =>
      @clickCallback() if @clickCallback

    accOffset = new paper.Point()
    # if $(window).width() < 640
    # if navigator.userAgent.indexOf('Mac OS X') == -1
    window.ondevicemotion = =>
      x = -event.accelerationIncludingGravity.x
      y = event.accelerationIncludingGravity.y + 5
      accOffset.x += (x-accOffset.x)*0.03
      accOffset.y += (y-accOffset.y)*0.03
      if @isLocked
        x = y = 0
        accOffset = new paper.Point()
      @offset.set (x-accOffset.x)*20, (y-accOffset.y)*20

    window.addEventListener 'mousemove', @mouseMove


  clear : ->
    # @cleanListeners()
    last = @shapes[@shapes.length-1]
    last.onMouseDown = null
    last.onMouseEnter = null
    last.onMouseLeave = null
    @currHighLightTween.stop false if @currHighLightTween
    @currHighLightTween = null
    @view.removeChildren()
    @shapes = []

  # cleanListeners : ->
  #   @view.onMouseDown = null
  #   window.ondevicemotion = null
  #   window.removeEventListener 'mousemove', @mouseMove


  lock : ->
    @offset.set 0,0
    @isLocked = true
    @isAnimating = false

  unlock : ->
    @isLocked = false

  setup : (@type, shapePts, palette, @bRotate=false)->

    @isLocked = false
    @currHighlighted = 0
    @currHighLightTween.stop false if @currHighLightTween

    # @bRotate = Math.random() < 0.4
    @amplitude = if @bRotate then 0.2 else 0.4

    scale = 1

    num = 10
    if $(window).width() < 640
      num = 5

    prev = null
    for i in [0..num]
      # shape = new paper.Path
      shape = new paper.Group
      shape.transformContent = false
      v = prev || @view
      v.addChild shape
      @shapes.push shape

      s = new paper.Path
      s.transformContent = false
      shape.addChild s
      for pt in shapePts
        s.add pt
      # shape.simplify()
      # shape.selected = true
      s.scale scale, scale
      scale *= 0.8

      prev = shape
    
    for i in [1..num]
      m = @shapes[i-1].children[0].clone()
      @shapes[i].addChild m
      @shapes[i].mask = m
      # m.selected = true
      m.clipMask = true

    # @mask = @view.children[0].clone()
    # @view.addChild @mask
    # @mask.clipMask = true

    if palette.length == 1
      @setTunnel palette
    else if palette.length == 2
      @setGradient palette[0], palette[1]
    else
      @setPalette palette


  animate : ->
    @currHighlighted = @currHighlighted || 1
    s = @shapes[@shapes.length-1-@currHighlighted]
    prevBrightness = s.children[0].fillColor.brightness
    @currHighLightTween = new TWEEN.Tween s.children[0].fillColor
    .to {brightness:3}, 70
    .onComplete =>
      new TWEEN.Tween s.children[0].fillColor
      .to {brightness:prevBrightness}, 80
      .start()
      if ++@currHighlighted >= @shapes.length 
        @currHighlighted=1
        setTimeout => 
          @animate() if @currHighLightTween
        ,2000
      else @animate()
    .start()


  setGradient : (c1, c2) ->
    c1 = new paper.Color c1
    c2 = new paper.Color c2
    color = c1.clone()
    num = @shapes.length
    for s,i in @shapes
      c = color.clone()
      c.red = ((c1.red*i) + (c2.red*(num-i)))/num
      c.green = ((c1.green*i) + (c2.green*(num-i)))/num
      c.blue = ((c1.blue*i) + (c2.blue*(num-i)))/num
      # c.setBrightness ((c1.brightness*i) + (c2.brightness*(num-i)))/num
      # c.setSaturation ((c1.saturation*i) + (c2.saturation*(num-i)))/num
      # c.setHue ((c1.hue*i) + (c2.hue*(num-i)))/num
      s.fillColor = c
    last = @shapes[@shapes.length-1]
    last.fillColor = c1


  setPalette : (cArr) ->
    num = @shapes.length
    for s,i in @shapes
      c = new paper.Color cArr[i%cArr.length]
      # r = (num-i)/num
      # c.type = 'hsb'
      # c.brightness *= r
      # c.saturation *= 0.5+r*0.5
      s.fillColor = c
    return

  setTunnel : (color) ->
    num = @shapes.length
    for s,i in @shapes
      c = new paper.Color color
      r = (num-i)/num
      c.type = 'hsb'
      c.brightness *= r
      # c.saturation *= 0.5+r*0.5
      s.fillColor = c
    return

  mouseMove : (e) =>
    return if @isLocked
    px = paper.view.bounds.width * 0.5
    py = paper.view.bounds.height * 0.5
    x = (e.x-px)*@amplitude
    y = (e.y-py)*@amplitude
    @offset.set x, y


  update : (dt) ->

    px = @initCenter.x
    py = @initCenter.y

    num = @shapes.length
    for shape,i in @shapes
      continue if i==0
      s = shape.children[0]
      dx = px - @offset.x * (i / num)
      dy = py - @offset.y * (i / num)
      s.position.x += (dx - s.position.x) * 0.05
      s.position.y += (dy - s.position.y) * 0.05

    for i in [1...@shapes.length]
      @shapes[i].mask.position = @shapes[i-1].children[0].position

    if !@bRotate
      return

    for shape,i in @shapes
      s = shape.children[0]
      continue if i==0
      s.rotation = @offset.x  / ((num-i)*0.5 + 0.5)

    for i in [1...@shapes.length]
      @shapes[i].mask.rotation = @shapes[i-1].children[0].rotation
    return


module.exports = Parallax
