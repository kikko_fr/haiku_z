fs            = require 'fs-extra'
path          = require 'path'
which         = require 'which'
chokidar      = require 'chokidar'
{spawn, exec} = require 'child_process'

green = '\x1B[0;32m'
reset = '\x1B[0m'



# -- SETTINGS --

lib_name     = 'main'
apps_path    = path.join 'node_modules','.bin'
build_file   = path.join 'build', lib_name + '.js'
out_file     = path.join 'bin', 'js', lib_name + '.js'
min_file     = path.join 'bin', 'js', lib_name + '.min.js'



# -- UTILS --

launch = (cmd, options) ->
  cmd = path.join apps_path, cmd
  prcss = spawn cmd, options
  prcss.stdout.pipe process.stdout
  prcss.stderr.pipe process.stderr

run = (cmd, options, callback) ->
  cmd = path.join apps_path, cmd
  opts = ' ' + options.join ' '
  prcss = exec cmd + opts, (error) -> 
    if error then console.log error
    else callback() if callback

compile = (callback) ->
  fs.ensureDirSync 'build'
  console.log 'compiling..'
  run 'coffee', ['-c', '-b', '-o', 'build', 'src/scripts'], ->
    log_done()
    callback() if callback

link = (callback) ->
  fs.ensureDirSync 'bin'
  console.log 'linking..'
  run 'browserify', ['-e', build_file, '-o', out_file], ->
    log_done()
    callback() if callback

log_done = -> console.log green + 'done.' + reset



# -- TASKS --

task 'dev', 'start dev env', ->
  launch 'stylus', ['-u', 'nib', '-w', 'src/stylesheets', '-o', 'bin/css']
  launch 'http-server', ['bin', '-s']
  watcher = chokidar.watch 'src/scripts', {persistent:true, ignoreInitial:true}
  watcher.on 'all', -> compile link
  compile link

task 'clean', 'clean builds', ->
  fs.removeSync 'build'
  # fs.removeSync 'bin'

task 'build',  'build lib', -> compile link

task 'test', 'run tests', ->
  launch 'mocha', ['--compilers', 'coffee:coffee-script/register']

task 'export', 'compile, link & export library', ->
  console.log 'minifying..'
  run 'uglifyjs', ['-o', min_file, out_file]
  log_done()